package pe.uni.v.caycho.jose.finalcc0a2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RelativeLayout relativeLayout;
    RadioButton rbTrue, rbFalse;
    Button btnNext, btnBack;
    ImageView imageView;
    TextView textView;

    ArrayList<Integer> images;
    ArrayList<String> questions;
    ArrayList<Boolean> correctChoices;
    boolean option;
    int iterator;
    final int NQUESTIONS = 6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question1);

        relativeLayout = findViewById(R.id.relative_layout);
        rbTrue = findViewById(R.id.radio_button_true);
        rbFalse = findViewById(R.id.radio_button_false);
        btnNext = findViewById(R.id.btn_next);
        btnBack = findViewById(R.id.btn_back);
        textView = findViewById(R.id.text_view);
        imageView = findViewById(R.id.image_view);

        images = new ArrayList<>();
        questions = new ArrayList<>();
        correctChoices = new ArrayList<>();
        iterator = 0;

        fillImages();
        fillQuestions();
        fillCorrectChoice();

        buttonEvents();
        radioButtonEvents();
    }

    private void fillImages() {
        images.add(R.drawable.imgo);
        images.add(R.drawable.imgx);
        images.add(R.drawable.imgcat);
        images.add(R.drawable.imglow);
        images.add(R.drawable.imgoff);
        images.add(R.drawable.imgon);
    }

    private void fillQuestions() {
        questions.add(getString(R.string.text_tv_q1));
        questions.add(getString(R.string.text_tv_q2));
        questions.add(getString(R.string.text_tv_q3));
        questions.add(getString(R.string.text_tv_q4));
        questions.add(getString(R.string.text_tv_q5));
        questions.add(getString(R.string.text_tv_q6));
    }

    private void fillCorrectChoice() {
        correctChoices.add(true);
        correctChoices.add(false);
        correctChoices.add(true);
        correctChoices.add(false);
        correctChoices.add(true);
        correctChoices.add(false);
    }

    private void buttonEvents() {
        btnNext.setOnClickListener(v -> {
            iterator++;
            if (iterator == NQUESTIONS) { // 0 1 2 3 4 5
                iterator = 0;
            }
            imageView.setImageResource(images.get(iterator));
            textView.setText(questions.get(iterator));
            rbTrue.setChecked(false);
            rbFalse.setChecked(false);
        });

        btnBack.setOnClickListener(v -> {
            if(iterator == 0) return;
            iterator--;
            imageView.setImageResource(images.get(iterator));
            textView.setText(questions.get(iterator));
        });
    }

    private void radioButtonEvents(){
        rbTrue.setOnClickListener(v -> {
            if(correctChoices.get(iterator))
                Snackbar.make(relativeLayout, R.string.text_correct, Snackbar.LENGTH_LONG).show();
            else
                Snackbar.make(relativeLayout, R.string.text_incorrect, Snackbar.LENGTH_LONG).show();
        });

        rbFalse.setOnClickListener(v -> {
            if(!correctChoices.get(iterator))
                Snackbar.make(relativeLayout, R.string.text_correct, Snackbar.LENGTH_LONG).show();
            else
                Snackbar.make(relativeLayout, R.string.text_incorrect, Snackbar.LENGTH_LONG).show();
        });
    }
}